let createError = require('http-errors');
let express = require('express');
let socketio = require('socket.io');
let http = require('http');

let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');

let indexRouter = require('./routes/index');
let usersRouter = require('./routes/users');

let moment = require('moment');

const {addUser, removeUser, getUser, getUsersInRoom, idleUsers} = require  ('./users.js');

const HISTORY_LIMIT = 30;  //0 no limit, -1 no history
const WARNING_TIME = 270;  // when user warned about idle
const DISCONNECT_TIME = 300;  //when user disconnected

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'twig');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


const MongoClient = require('mongodb').MongoClient;
let db1;
const uri = "mongodb+srv://chat:chatdbpass@cluster0-0evpw.mongodb.net/test"
MongoClient.connect(uri, function(err, client) {
  if(err) {
    console.log('Error occurred while connecting to MongoDB Atlas...\n',err);
  }
  console.log('Connected...');
  db1 = client.db("chat");
  //client.close();
});




const PORT = process.env.PORT || 3001
const server = http.createServer(app);
const io = socketio(server);
server.listen(PORT, ()=> console.log(`Server started on port ${PORT}`));


io.on('connect', (socket) => {
  console.log('User connected: '+socket.id);

  socket.on('join', ({ name, room }, callback) => {

    const { error, user } = addUser({ id: socket.id, name, room});

    if(error) return callback(error);
    socket.join(user.room);

    let timestamp = Date.now();
    let date = moment(timestamp).format("MM/DD/YYYY");
    let time = moment(timestamp).format("H:mm");
    let db_messages;
    db1.collection("messages").find({}).sort({ $natural: -1 }).limit(HISTORY_LIMIT).toArray(function(err, result) {
      if (err) throw err;
      db_messages = result.reverse();
      db_messages.forEach(function callback(db_message, index, array){
        socket.emit('message', db_message);
      });

      socket.emit('message', { user: 'Admin', text: `Welcome ${user.name}`, date:date, time:time});
    });


    let obj_message =
        { user: 'Admin', text: `${user.name} has joined`, date:date, time:time }
     db1.collection("messages").insertOne(obj_message, function(err, res) {
      if (err) throw err;
       socket.broadcast.to(user.room).emit('message', obj_message);
       io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room) });
       console.log('in room #' + user.room + ': ' +getUsersInRoom(user.room).length + ' online');
       console.log(name+' joined #'+room);
     });

    callback();
  })

    socket.on('sendMessage', (message, callback)=>{
      const { error, user } = getUser(socket.id);
      if(error) return callback(error);
      let timestamp = Date.now();
      let date = moment(timestamp).format("MM/DD/YYYY");
      let time = moment(timestamp).format("H:mm");
      user.lastMessage = timestamp;
      user.warned = false;
      let obj_message =
        { user: user.name, text: message.substring(0,1000), date:date, time:time, timestamp:timestamp, id:timestamp }
      db1.collection("messages").insertOne(obj_message, function(err, res) {
        if (err) throw err;
        io.to(user.room).emit('message', obj_message);
        console.log(`${user.name}: ${message} - room:${user.room}`);
      });

      callback();
    })

  socket.on('disconnect',()=>{
    const  user = removeUser(socket.id);
    let timestamp = Date.now();
    let date = moment(timestamp).format("MM/DD/YYYY");
    let time = moment(timestamp).format("H:mm");

    if(user) {
      let obj_message =
          { user: 'Admin', text: `${user.name} has left.`, date:date, time:time, id:timestamp }
      db1.collection("messages").insertOne(obj_message, function(err, res) {
        if (err) throw err;
        console.log(`User [${user.name}] disconnected.`);
        io.to(user.room).emit('message', obj_message);
        io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room), date:date, time:time, id:timestamp});
      });

    }
  })

});


function EverySecond(i) {
  setTimeout(() => {

    let timestamp = Date.now();
    let date = moment(timestamp).format("MM/DD/YYYY");
    let time = moment(timestamp).format("H:mm");

    let users = idleUsers(WARNING_TIME,false);
    users.forEach(function callback(user, index, array){
      if (io.sockets.sockets[user.id]) {
        io.sockets.connected[user.id].emit('message', {user: 'Admin', text: `you will be disconnected due inactivity in 30 seconds`, date:date, time:time, id:timestamp });
        users[index].warned=true;
      }
    });



    users = idleUsers(DISCONNECT_TIME,true);
    users.forEach(function callback(user, index, array){
      let obj_message =
          { user: 'Admin', text: `${user.name} disconnected due inactivity.`, date:date, time:time, id:timestamp }
      db1.collection("messages").insertOne(obj_message, function(err, res) {
        if (err) throw err;
        console.log(`User [${user.name}] disconnected.`);
        io.to(user.room).emit('message', obj_message);
        io.to(user.room).emit('roomData', { room: user.room, users: getUsersInRoom(user.room), date:date, time:time, id:timestamp});
        removeUser(user.id);
        if (io.sockets.sockets[user.id]) {
          io.sockets.connected[user.id].emit('disconnect_activity', {reason:"disconnected due inactivity"});
          io.sockets.sockets[user.id].disconnect();
        }
      });
    });

    EverySecond(++i);
  }, 1000)
}

EverySecond(0);





module.exports = app;

