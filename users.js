const users = [];

const addUser = ({id, name, room, lastMessage, warned}) =>{
    if (!name) return {error: 'Username cannot be empty'};
    name = name.trim().replace(/[^a-zа-я0-9_]/gi,'').substring(0,25);
    //else return {error: 'Username cannot be empty'};
    room = room.trim().toLowerCase();
    const existingUser = users.find((user)=> user.room === room && user.name === name );
    if (existingUser){
        return {error: 'Username is taken'};
    }
    if (name.toLowerCase()==="admin"){
        return {error: 'You cannot be Admin:)'};
    }

    const user = {id, name, room, lastMessage: Date.now(), warned:false};
    users.push(user);
    return { user };
}

const removeUser = (id) =>{
    const index = users.findIndex((user) => user.id === id);
    if (index!==-1){
        return users.splice(index,1)[0];
    }
}

const getUser = (id) =>{
    let user =  users.find((user)=>user.id===id);
    if (!user){
        return {error: 'User undefined'};
    }
    return { user };
}

const getUsersInRoom = (room) =>{
    return users.filter((user)=>user.room===room);
}

const idleUsers = (seconds, warned) => {
    let timestamp = Date.now();
    return  users.filter((user)=>user.lastMessage<timestamp-seconds*1000 && user.warned===warned);
}

module.exports = {addUser, removeUser, getUser, getUsersInRoom, idleUsers}